import { Request, Response } from "express";
import { NextFunction } from "connect";
const dirName = require('path').resolve(__dirname, '..');
import i18n from "i18n";

i18n.configure({
    locales: ['en'], // setup some locales - other locales default to en silently
    directory: dirName + '/src/locales', // where to store json files - defaults to './locales' relative to modules directory
    defaultLocale: 'en',
    register: global
});
export default function (req: Request, res: Response, next: NextFunction) {
    i18n.init(req, res);
    return next();
}