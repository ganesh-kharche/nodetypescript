const env = process.env.NODE_ENV || "development";
require('custom-env').env();
export const config = {
	dbString: process.env.DB_URL?.toString() || '',
	httpPort: process.env.HTTP_PORT || '4000',
	JWT_PRIVATE_KEY: process.env.CSV_URL || '',
	CSV_URL: process.env.CSV_URL || ''
};