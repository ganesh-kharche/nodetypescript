export interface IUsers {
    firstName?: string;
    lastName?: string;
    userName?: string;
    startDate: number;
    endDate: number;
}