import { fs, path } from '../libraries';

export const createDir = async (dir: string) => {
    const basepath = __basedir;
    const pathToCreate = path.join(basepath, `${dir}`);
    if (!fs.existsSync(pathToCreate)) {
        fs.mkdirSync(pathToCreate);
    }
}
