import { ObjectID } from '../libraries';
import { Schema, Document, model } from "mongoose";
import { IUsers } from './interfaces/users';

const schema = new Schema({
	firstName: {
		type: String,
		required: true,
		trim: true,
		lowercase: true
	},
	lastName: {
		type: String,
		required: true,
		trim: true,
		lowercase: true
	},
	userName: {
		type: String,
		required: true,
		trim: true,
		lowercase: true
	},
	startDate: {
		type: Number,
		default: new Date().getTime()
	},
	endDate: {
		type: Number 
	},
});

export interface IUsersDocument extends Document, IUsers {
	_id: ObjectID
}

export const UsersSchema = model<IUsersDocument>('Users', schema);