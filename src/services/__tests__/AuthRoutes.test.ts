jest.mock('../auth_service');
jest.mock('csv-writer', () => {
    const mCsv = {
        createObjectCsvWriter: jest.fn().mockReturnThis(),
    };
    return jest.fn(() => mCsv);
});

import testAppllicationApp from '../../server';
import { expect } from '@jest/globals';
import request from "supertest";
import AuthService from '../auth_service';

beforeAll(() => {
    jest.useFakeTimers();
})

describe('GET /search', () => {
    it('should return 200', async () => {
        await AuthService.findUserByDate(1602227005000, 1602313340000);
        const createCsvWriter = (() => jest.fn())
        createCsvWriter().mockReturnValue(true);
        const result = await request(testAppllicationApp)
            .get(`/search?startDate=1602227005000&endDate=1602313340000`)
            .expect('Content-Type', 'text/csv')

        expect(result.body).toEqual(true);
        expect(result.statusCode).toEqual(200);
    })
});

describe("POST / - a simple api endpoint", () => {
    it("Create User API Request", async () => {
        const mockData = {
            "firstName": "afdf",
            "lastName": "lsdfjkd",
            "userName": "sam",
            "startDate": 1602227005000,
            "endDate": 1602313340000
        };
        await AuthService.createUser(mockData);
        const result = await request(testAppllicationApp)
            .post("/")
            .send(mockData);
        expect(result.body.success).toEqual(true);
        expect(result.statusCode).toEqual(200);
    });
});