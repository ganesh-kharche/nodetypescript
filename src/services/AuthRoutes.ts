import localService from './auth_service';
import { IUsers } from './interfaces/users';
const json2csv = require('json2csv').parse;
import {
    Router, 
    Request, 
    Response,
    NextFunction,
    SuccessInputs,
    SchemaValidator
} from '../libraries';
import joiValidator from '../schemas/auth';
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const router = Router();

router.get('/search', SchemaValidator(joiValidator.userDetails), async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user: IUsers = JSON.parse(JSON.stringify(req.query));
        
        const data = JSON.parse(JSON.stringify(await localService.findUserByDate(user.startDate, user.endDate)));
        const csvWriter = createCsvWriter({
            path: './public/output.csv',
            header: [
                {id: '_id', title: 'ID'},
                {id: 'firstName', title: 'FIRSTNAME'},
                {id: 'lastName', title: 'LASTNAME'},
                {id: 'userName', title: 'USERNAME'},
                {id: 'startDate', title: 'STARTDATE'},
                {id: 'endDate', title: 'ENDDATE'},
            ]
        });
        const csvString = json2csv(data);
        await csvWriter.writeRecords(data);
        res.setHeader('Content-disposition', 'attachment; filename=output.csv');
        res.set('Content-Type', 'text/csv');
        res.status(200).send(csvString);
        
    } catch (e) {
        return next(e)
    }
});

router.post('/', async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user: IUsers = req.body;
        const data = await localService.createUser(user);
        throw new SuccessInputs(true, res.__('data-created-successfully'), data);
    } catch (e) {
        return next(e)
    }
});

export default router;