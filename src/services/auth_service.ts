import { UsersSchema } from './schema';
import { IUsers } from './interfaces/users';

class AuthService {
    async findUserByDate(startDate: number, endDate: number) {
        try {
            return (await UsersSchema.find({
                startDate: { $gte: startDate },
                endDate: { $lte: endDate }
            }, '-__v'));
        } catch (error) {
            return false;
        }
    }

    async createUser(user: IUsers) {
        try {
            return (await UsersSchema.create(user));
        } catch (error) {
            return false;
        }
    }
}



export default new AuthService();