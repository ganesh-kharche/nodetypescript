import { 
	Request, 
	Response, 
	NextFunction
} from '../libraries';

export const authenticationUser = async function (req: Request, res: Response, next: NextFunction) {
	try {
		return next();
	} catch (e) {
		return next(e);
	}
}