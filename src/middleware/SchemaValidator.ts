/* middlewares/SchemaValidator.js */
import {
    Request, Response, NextFunction, InvalidInputsError
} from '../libraries';
 
export default (schema: any) => {
    const _supportedMethods = ['post', 'put', 'get', 'patch', 'delete'];
    const _validationOptions = {
        abortEarly: false, // abort after the last validation error
        allowUnknown: false, // allow unknown keys that will be ignored
        stripUnknown: true // remove unknown keys from the validated data
    };

    // return the validation middleware
    return (req: Request, res: Response, next: NextFunction) => {
        const method = req.method.toLowerCase();
        if (_supportedMethods.includes(method)) {
            if (schema.headers) {
                const err = schema.headers.validate(req.headers, '', _validationOptions);
                if (err && err.error) handleError(err, res);
            }

            if (schema.params) {
                const err = schema.params.validate(req.params, '', _validationOptions);
                if (err && err.error) handleError(err, res);
            }

            if (schema.query) {
                const err = schema.query.validate(req.query, '', _validationOptions);
                if (err && err.error) handleError(err, res);
            }

            if (schema.body) {
                const err = schema.body.validate(req.body,'' ,_validationOptions);
                if (err && err.error) handleError(err, res);
            }
            return next();
        }
        return next();
    };
};

function handleError(err: any, res: any) {
    const msg = err && err.error && err.error.details && err.error.details[0].message ? err.error.details: err
    throw new InvalidInputsError(res.__('Invalid-Input-Error'), res.__('invalid-input'), msg);
}