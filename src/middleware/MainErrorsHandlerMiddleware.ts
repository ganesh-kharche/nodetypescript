import { NextFunction, Request, Response } from "express";
import { PageNotFoundError } from "../errors/PageNotFoundError";
import { InvalidInputsError } from "../errors/InvalidInputsError";
import { InvalidInputError } from "../errors/InvalidInputError";
import { SuccessInputs } from "../helper/SuccessInputs";

export const MainErrorsHandlerMiddleware = function (error: any, req: Request, res: Response, next: NextFunction) {

	switch (error.constructor) {

		case InvalidInputsError: {
			return res.status(400).json({
				type: (<InvalidInputsError>error).type,
				message: error.message,
				errors: (<InvalidInputsError>error).errors
			}).end();
		}
		case InvalidInputError: {
			return res.status(400).json({
				type: (<InvalidInputsError>error).type,
				message: error.message
			}).end();
		}
		case PageNotFoundError: {
			return res.status(404).json(error.message).end();
		}
		case SuccessInputs: {
			return res.status(200).json({
				success: error.success,
                message: error.message,
                data: error.data
			}).end();
		}
		default: {
			if (error && error.message && error.errors) {
				return res.status(400).json({
					type: 'Validation Failed',
					message: error.message,
					errors: (<InvalidInputsError>error).errors
				}).end();
			} else if (error && error.name === 'TokenExpiredError') {
				return res.status(400).json({
					type: 'Invalid-token',
					message: 'Authorization token expired.'
				}).end();
			} else {
				return res.status(500).json('Internal Server Error').end();
			}
			
		}

	}
};