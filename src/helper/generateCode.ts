export default async () => {
	let code = null;
	do {
		code = Math.floor(Math.random() * 10000);
	} while (code < 1000);
	return code;
}
