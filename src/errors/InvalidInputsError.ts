export class InvalidInputsError extends Error {
    readonly errors: {};
    readonly type:string;
    constructor(message:string, type:string, errors:{}) {
        super(message);
        this.errors = errors;
        this.type = type;
    }
}