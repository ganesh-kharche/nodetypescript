export class InvalidInputError extends Error {
    readonly type: string;
    constructor(message: string, type: string) {
        super(message);
        this.type = type;
    }
}