import { Joi } from '../libraries';
const userDetails = {
    query: Joi.object({
        startDate: Joi.number().required(),
        endDate: Joi.number().required()
    })
}

export default {
    userDetails
}