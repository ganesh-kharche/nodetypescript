import { express, mongoose, bodyParser, config, i18n, path} from "./libraries";
import { MainErrorsHandlerMiddleware } from "./middleware/MainErrorsHandlerMiddleware";
import AuthRoutes from "./services/AuthRoutes";
declare global {
    const __basedir: string;
}

const testAppllicationApp: express.Application = express();
testAppllicationApp.use(bodyParser.urlencoded({ extended: false }))
.use(bodyParser.json())
// @ts-ignore
.use(i18n)
.use(require("cors")())
.use('/', AuthRoutes)
// @ts-ignore
global.__basedir = path.resolve(__dirname, '..');
mongoose.connect(config.dbString, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true }).then((obj) => {
    mongoose.set('useCreateIndex', true);
    console.log(`Server Connecting to Mongoo.`);
	const port: number = +config.httpPort;
	testAppllicationApp
    .use(express.static(__basedir + '/public'))
    .use(MainErrorsHandlerMiddleware)
    .listen(port, () => console.log(`Server Listening on port ${port}`));
}).catch((error) => console.log(`Connection to Mongoo DB Failed. ${error}`));

export default testAppllicationApp;