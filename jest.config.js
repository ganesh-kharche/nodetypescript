module.exports = {
    roots: ['<rootDir>/src'],
    preset: 'ts-jest',
    testEnvironment: 'node',
    moduleNameMapper: {
        "test/(.*)": "<rootDir>/test/$1",
    },
    collectCoverage: true,
    collectCoverageFrom: [
        "src/**/*.{js,ts}",
    ],
    coverageDirectory: "coverage",
    verbose: true,
    "transform": {
        "node_modules/variables/.+\\.(j|t)sx?$": "ts-jest"
    },
    "transformIgnorePatterns": [
        "node_modules/(?!variables/.*)"
    ]
    // coverageThreshold: {
    //     global: {
    //         branches: 100,
    //         functions: 100,
    //         lines: 100,
    //         statements: 100
    //     }
    // }
};